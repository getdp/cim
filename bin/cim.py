#!/usr/bin/env python2
"""
cim.py, a non-linear eigenvalue solver.
Copyright (C) 2017 N. Marsic, F. Wolf, S. Schoeps and H. De Gersem,
Institut fuer Theorie Elektromagnetischer Felder (TEMF),
Technische Universitaet Darmstadt.

See the LICENSE.txt and README.md for more license and copyright information.
"""

import solver as Solver
import beyn   as Beyn
import numpy  as np
import args   as args
import time   as timer

# Start timer
tStart = timer.time()

# Parse arguments
arg = args.parse()

# Prepare variables of -setnumber for GetDP
setnumber = []
for i in arg.setnumber:
    setnumber.append("-setnumber")
    for j in i:
        setnumber.append(j)

# Initialise GetDP
operator = Solver.GetDPWave(arg.pro, arg.mesh, arg.resolution, setnumber)

# Compute
l, V, r = Beyn.simple(operator, arg.origin, arg.radius,
                      arg.nodes, arg.maxIt, arg.lStart, arg.lStep, arg.rankTol,
                      not arg.quiet)

# Display the computed eigenvalues
if(not arg.quiet):
    print
    print "Eigenvalues:"
    print "----------- "

for i in range(l.shape[0]):
    if(not arg.quiet): print " # " + str(i) + ": ",
    print "(" + format(np.real(l[i]).tolist(), '+.12e') + ")",
    print "+",
    print "(" + format(np.imag(l[i]).tolist(), '+.12e') + ")*j",
    print "| " + format(r[i], 'e'),
    print "| " + format(r[i]/np.abs(l[i]), 'e')

if(not arg.quiet):
    print "----------- "
    print "Found: " + str(l.shape[0])

# Generate GetDP views for eigenvectors
if(not arg.quiet):
    print
    print "Generating eigenvector views..."

for i in range(l.shape[0]):
    operator.view(V[:, i], "cim" + str(i) + ".pos")
    if(not arg.quiet): print " # View: " + str(i) + " done!"

# Stop timer
tStop = timer.time()

# Done
if(not arg.quiet):
    print
    print "Elapsed time: " + str(round(tStop - tStart)) + "s"
    print "Bye!"
