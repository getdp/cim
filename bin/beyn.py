"""
cim.py, a non-linear eigenvalue solver.
Copyright (C) 2017 N. Marsic, F. Wolf, S. Schoeps and H. De Gersem,
Institut fuer Theorie Elektromagnetischer Felder (TEMF),
Technische Universitaet Darmstadt.

See the LICENSE.txt and README.md for more license and copyright information.
"""

import solver as Solver
import sys
import numpy  as np
import numpy.matlib

def simple(operator, origin, radius,
           nNode=100, maxIt=10, lStart=4, lStep=3, rankTol=1e-4, verbose=True):
    """Solves an eigenvalue problem using Beyn's algorithm (simple version)

    Keyword arguments:
    operator -- the solver defining the operator to use
    origin -- the origin (in the complex plane) of the above circular contour
    radius -- the radius of the circular contour used to search the eigenvalues
    nNode -- the number of nodes for the trapezoidal integration rule (optional)
    maxIt -- the maximal number of iteration for constructing A0 (optional)
    lStart -- the number of columns used for A0 when algorithm starts (optional)
    lStep -- the step used for increasing the number of columns of A0 (optional)
    rankTol -- the tolerance for the rank test (optional)
    verbose -- should I be verbose? (optional)

    Returns the computed eigenvalues, eigenvectors
    and the associate *absolute* residual norms
    """

    # Display the parameter used
    if(verbose): display(nNode, maxIt, lStart, lStep, rankTol, origin, radius)

    # Initialise A0 search
    hasK = False
    it   = 0
    m    = operator.size()
    l    = lStart
    k    = -1

    # Search A0
    if(verbose): print "Searching A0 and A1 (" + str(m) + " unknowns)..."
    while(not hasK and it != maxIt):
        if(verbose): print " # Iteration " + str(it+1) + ": ",
        if(verbose): sys.stdout.flush()

        vHat   = randomMatrix(m, l)                     # Take a random VHat
        A0, A1 = integrate(operator, vHat,              # Compute A_0 and A_1
                           nNode, radius, origin)
        k, sV  = rank(A0, rankTol)                      # Rank

        if(verbose): print       format(sV[0],  ".2e") + " |",
        if(verbose): print       format(sV[-1], ".2e"),
        if(verbose): print "[" + format(k,        "d") + "]"

        if(k == 0):                                     # Rank is zero?
            if(verbose): print " # Rank test is zero: keep A0 as is!"
            hasK = True                                 # -> Keep A0 (and warn)
        elif(k == m):                                   # Maximum rank reached?
            if(verbose): print " # Maximum rank reached: keep A0 as is!"
            hasK = True                                 # -> Keep A0 (and warn)
        elif(k < l):                                    # Found a null SV?
            hasK = True                                 #  -> We have A0
        else:                                           # Matrix is full rank?
            l = l + lStep                               #  -> Increase A0 size

        it += 1                                         # Keep on searching A0

    # Check if maxIt was reached
    if(it == maxIt):
        l = l - lStep
        if(verbose): print "# Last iteration over: keep A0 as is!"

    # Display singular values
    if(verbose): print "Singular values: " + ", ".join("%.2e" % i for i in sV)

    # Compute V, S and Wh
    #  NB: For SVD(A) = V*S*Wh, numpy computes {v, s, w}, such that:
    #      v = V; diag(s) = S and w = Wh
    if(verbose): print "Constructing linear EVP..."
    V, S, Wh = np.linalg.svd(A0, full_matrices=False, compute_uv=1)

    # Extract V0, W0 and S0Inv
    V0    =    V[np.reshape(range(0, m), (-1, 1)), range(0, k)]
    W0    = Wh.H[np.reshape(range(0, l), (-1, 1)), range(0, k)]
    S0Inv = np.matrix(np.diag(1/S[0:k]))

    # Compute B
    B = V0.H * A1 * W0 * S0Inv

    # Eigenvalues & eigenvectors
    if(verbose): print "Solving linear EVP..."
    myLambda, QHat = numpy.linalg.eig(B)
    Q = V0 * QHat;

    # Absolute residual norm
    nFound = myLambda.shape[0]
    norm   = np.empty((nFound))
    for i in range(nFound):
        operator.apply(Q[:, i], myLambda[i]) # Apply eigenpair
        r = operator.solution(0)             # Get residual
        norm[i] = np.linalg.norm(r)          # Save the norm

    # Done
    if(verbose): print "Done!"
    return myLambda, Q, norm


## Import only simple (other functions are just helpers)
__all__ = ["simple"]


## Helper functions
def rank(A, tol):
    """Returns the rank of a given matrix and its singular values

    Keywords arguments:
    A -- the matrix to use
    tol -- the relative tolerance used to compute the rank
    """

    S   = np.linalg.svd(A, full_matrices=False, compute_uv=0)
    nSV = len(S)
    k   = 0

    for i in range(nSV):
        if(S[i]/S[0] > tol): k = k + 1

    return k, S.tolist()


def integrate(operator, vHat, nNode, radius, origin):
    """Computes the two first countour integrals of Beyn's method (A_0 and A_1)
    over a circular contour.

    Keyword arguments:
    operator -- the solver defining the operator to use
    vHat -- the RHS matrix defing Beyn's integrals
    nNode -- the number of nodes used to discretise the circular contour
    radius -- the radius of the circular contour
    origin -- the origin (in the complex plane) of the circular contour
    """

    # Initialise integrals
    A0 = np.matlib.zeros(vHat.shape, dtype=complex)
    A1 = np.matlib.zeros(vHat.shape, dtype=complex)

    # Loop over integation points and integrate
    for i in range(nNode):
       t   = 2 * np.pi * i / nNode;
       phi = origin + radius * np.exp(1j * t)
       tmp = multiSolve(operator, vHat, phi)

       A0 += tmp * np.power(phi, 0) * np.exp(1j * t)
       A1 += tmp * np.power(phi, 1) * np.exp(1j * t)

    # Final scale
    A0 *= radius/nNode
    A1 *= radius/nNode

    # Done
    return A0, A1


def multiSolve(solver, B, w):
    """Solves for multiple RHS

    Keyword arguments:
    solver -- the solver to use
    B -- the matrix of RHS
    w -- the complex frequency to use
    """

    # Solve
    solver.solve(B, w)

    # Get solutions
    nSolve = B.shape[1]
    size   = solver.size()
    X      = np.matlib.empty((size, nSolve), dtype=complex)
    for i in range(nSolve):
        X[:, i] = solver.solution(i)

    # Done
    return X


def randomMatrix(n, m):
    """Returns a random complex matrix of the given size (n x m)"""
    return np.matlib.rand(n, m) + np.matlib.rand(n, m) * 1j


def display(nNode, maxIt, lStart, lStep, rankTol, origin, radius):
    print "Beyn's contour integral method (simple)"
    print "---------------------------------------"
    print " # Nodes used for the trapezoidal rule:" + " " + str(nNode)
    print " # Maximum number of iterations:       " + " " + str(maxIt)
    print " # Initial size of col(A0):            " + " " + str(lStart)
    print " # Step size for increasing col(A0):   " + " " + str(lStep)
    print " # Rank test relative tolerance:       ",
    print format(rankTol, ".2e")
    print "---------------------------------------"
    print " # Cirular path origin:                ",
    print "(" + format(origin.real, "+.2e") + ")",
    print "+",
    print "(" + format(origin.imag, "+.2e") + ")j"
    print " # Cirular path radius:                ",
    print format(radius, "+.2e")
    print "---------------------------------------"
