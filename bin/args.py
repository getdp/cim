"""
cim.py, a non-linear eigenvalue solver.
Copyright (C) 2017 N. Marsic, F. Wolf, S. Schoeps and H. De Gersem,
Institut fuer Theorie Elektromagnetischer Felder (TEMF),
Technische Universitaet Darmstadt.

See the LICENSE.txt and README.md for more license and copyright information.
"""

import argparse

def parse():
    """Parses python script arguments

    Returns an argparse
    """
    # Initialise
    parser = argparse.ArgumentParser(prog="cim.py",
                                     description=desc,
                                     formatter_class=MyFormatter,
                                     epilog="authors: " +
                                            "N. Marsic, F. Wolf, " +
                                            "S. Schoeps and H. De Gersem")

    # Position argumets
    parser.add_argument("pro", type=str,
                        help="GetDP .pro file")
    parser.add_argument("mesh", type=str,
                        help="Gmsh .msh file")
    parser.add_argument("resolution", type=str,
                        help="resolution from .pro file")
    parser.add_argument("origin", type=complex,
                        help="circular contour origin (complex)")
    parser.add_argument("radius", type=float,
                        help="circular contour radius")

    # Optional arguments
    parser.add_argument("-nodes", type=int, default=100,
                        help="number of nodes for trapezoidal rule")
    parser.add_argument("-maxIt", type=int, default=10,
                        help="maximum number of iterations")
    parser.add_argument("-lStart", type=int, default=4,
                        help="initial size of col(A0)")
    parser.add_argument("-lStep", type=int, default=3,
                        help="step size for increasing col(A0)")
    parser.add_argument("-rankTol", type=float, default=1e-4,
                        help="relative tolerance for rank test")
    parser.add_argument("-setnumber", type=str, default=[], action='append',
                        nargs=2, metavar=('NAME', 'VALUE'),
                        help="set constant number NAME=VALUE")
    parser.add_argument("-quiet", action='store_true',
                        help="should I be quiet?")

    # Done
    return parser.parse_args()


## Import only parse (other functions are just helpers)
__all__ = ['parse']


## Helper
desc = ("a non-linear eigenvalue solver " +
        "using the contour integral method proposed by W.-J. Beyn " +
        "coupled with a GetDP formulation.")


class MyFormatter(argparse.HelpFormatter):
    """ Custom Formatter"""

    def __init__(self, prog):
        super(MyFormatter, self).__init__(prog,
                                          indent_increment=2,
                                          max_help_position=30,
                                          width=None)

    def _get_help_string(self, action):
        help = action.help
        if '%(default)' not in action.help:
            if action.default is not argparse.SUPPRESS:
                defaulting_nargs = [argparse.OPTIONAL, argparse.ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' (default: %(default)s)'
        return help
