Group{
  Omega = Region[1]; // Domain
  Bndry = Region[2]; // Boundary
}

Function{
  // Physical data //
  C0 = 299792458; // [m/s]

  // Eigenvalue solver data //
  DefineConstant[nEig   = 10,    // [-]
                 target = 8e17]; // [(rad/s)^2]
}

Jacobian{
  { Name JVol;
    Case{
      { Region All; Jacobian Vol; }
    }
  }
}

Integration{
  { Name IP2;
    Case{
      { Type Gauss;
        Case{
          { GeoElement Line;        NumberOfPoints 2; }
          { GeoElement Triangle;    NumberOfPoints 3; }
          { GeoElement Tetrahedron; NumberOfPoints 4; }
        }
      }
    }
  }
}

Constraint{
  { Name Dirichlet; Type Assign;
    Case{
      { Region Bndry; Value 0; }
    }
  }
}

FunctionSpace{
  { Name HCurl; Type Form1;
    BasisFunction{ // Nedelec
      { Name se; NameOfCoef ee; Function BF_Edge;
        Support Omega; Entity EdgesOf[All]; }
    }

    Constraint{
      { NameOfCoef ee; EntityType EdgesOf; NameOfConstraint Dirichlet; }
    }
  }
}

Formulation{
  { Name Eig; Type FemEquation;
    Quantity{
      { Name e; Type Local; NameOfSpace HCurl; }
    }
    Equation{
      Galerkin{        [         Dof{d e} , {d e}];
        In Omega; Integration IP2; Jacobian JVol; }
      Galerkin{ DtDtDof[1/C0^2 * Dof{  e} , {  e}];
        In Omega; Integration IP2; Jacobian JVol; }
    }
  }
}

Resolution{
  { Name Eig;
    System{
      { Name A; NameOfFormulation Eig; Type Complex; }
    }
    Operation{
      GenerateSeparate[A];
      EigenSolve[A, nEig, target, 0];
    }
  }
}

PostProcessing{
  { Name Eig; NameOfFormulation Eig;
    Quantity{
      { Name e; Value{ Local{ [{e}]; In Omega; Jacobian JVol; } } }
    }
  }
}

PostOperation{
  { Name Eig; NameOfPostProcessing Eig;
    Operation{
      Print[e, OnElementsOf Omega, File "eig.pos", EigenvalueLegend];
    }
  }
}
