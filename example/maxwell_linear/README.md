This is a simple linear eigenvalue problem example. It consists in a rectangular
electromagnetic cavity. Simply run:

    make cim

to test cim.py.
The [Makefile](Makefile) will first mesh the geometry [square.geo](square.geo)
by calling [Gmsh](http://gmsh.info).
Afterwards, cim.py is called. The [GetDP](http://getdp.info) formulation is
located in [maxwell.pro](maxwell.pro). In order to check the solution,
simply run:

    make ref

This will solve the linear eigenvalue problem with a classical algorithm. This
resolution is implemented in [ref.pro](ref.pro). If you use [GetDP](
http://getdp.info) with [SLEPc](http://slepc.upv.es), the default algorithm
should be [Krylov-Schur](https://dx.doi.org/10.1137/S0895479800371529).
