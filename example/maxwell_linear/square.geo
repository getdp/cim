DefineConstant[l   = {01, Name "Input/00Geometry/00Length [m]"},
               msh = {10, Name "Input/01Mesh/00Density [point per length]"}];

Point(1) = {+l/2, -l/2, 0, 1};
Point(2) = {+l/2, +l/2, 0, 1};
Point(3) = {-l/2, +l/2, 0, 1};
Point(4) = {-l/2, -l/2, 0, 1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line     Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

Transfinite    Line {1, 2, 3, 4} = (msh * l) Using Progression 1;
Transfinite Surface {1};

Physical Surface(1) = {1};
Physical    Line(2) = {1, 2, 3, 4};
