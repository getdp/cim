/////////////////////////////////////////////////////////////////////
// Resolution for contour integral method.                         //
// Warning, the master file should include "cimParameters.pro",    //
// and should also define "FormulationCIM" the formulation to use. //
/////////////////////////////////////////////////////////////////////
Resolution{
  { Name Solve;
    System{ { Name A; NameOfFormulation FormulationCIM; Type ComplexValue; } }
    Operation{
      Generate[A];

      If(doInit)
        CopySolution[A, x~{0}()];
      EndIf

      If(doSolve)
        // Full solve for first RHS
        CopyRightHandSide[b~{0}(), A];
        Solve[A];
        CopySolution[A, x~{0}()];

        // SolveAgain for other RHSs
        For i In {1:nRHS-1}
          CopyRightHandSide[b~{i}(), A];
          SolveAgain[A];
          CopySolution[A, x~{i}()];
        EndFor
      EndIf

      If(doApply)
        CopySolution[x~{0}(), A];
        Apply[A];
        CopySolution[A, x~{0}()];
      EndIf

      If(doPostpro)
        CopySolution[x~{0}(), A];
        PostOperation[Post];
      EndIf
    }
  }
}
