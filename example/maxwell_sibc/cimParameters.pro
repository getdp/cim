/////////////////////////////////////////////////////////////////
// Parameters for contour integral method.                     //
// The master file should inclue "cimResolution.pro" later on. //
/////////////////////////////////////////////////////////////////
Function{
  // Physical data //
  DefineConstant[angularFreqRe = 1,  // [rad/s]
                 angularFreqIm = 0]; // [rad/s]

  Puls[]  = Complex[angularFreqRe,
                    angularFreqIm];  // [rad/m]

  // Algebraic data //
  DefineConstant[nRHS = 1];       // Number of RHS for this run
  For i In {0:nRHS-1}
    DefineConstant[x~{i}() = {},  // Solution
                   b~{i}() = {}]; // Right hand side
  EndFor

  // Control data //
  DefineConstant[doInit    = 0,          // Should I initialize some stuff?
                 doSolve   = 0,          // Should I solve Ax = b?
                 doPostpro = 0,          // Should I only create a view for x()?
                 doApply   = 0,          // Should I only apply x(): x <- Ax?
                 fileName  = "eig.pos"]; // Postpro file name
}
