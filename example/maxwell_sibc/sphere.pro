Include "sphere.dat";

Group{
  Omega = Region[1];
  Bndry = Region[2];
}

Include "./cimParameters.pro"; // Functions for cim.py

Function{
  // Imaginary Unit //
  J[] = Complex[0, 1];

  // Physical Constants //
  C0   = 299792458;      // [m/s]
  Mu0  = 4*Pi*1e-7;      // [H/m]
  Eps0 = 1/(Mu0 * C0^2); // [F/m]

  // Leontovich SIBC (H-Formulation) //
  If(isSC == 0)
    SL[] = Sqrt[(J[] * Puls[] * Mu0) / Sigma];   // H-Formulation
  EndIf
  If(isSC == 1)
    SL[] = 0.5 * Puls[]^2 * Mu0^2 * Lambda^3 * Sigma +
           J[] * Puls[]   * Mu0   * Lambda;      // H-Formulation
  EndIf
  SIbc[] = -1.0 / SL[];                          // E-Formulation
}

Integration{
  { Name I1;
    Case{
      { Type Gauss;
        Case{
          { GeoElement Tetrahedron; NumberOfPoints 4; } // O1 * O1
          { GeoElement Triangle;    NumberOfPoints 3; } // O1 * O1
          { GeoElement Line;        NumberOfPoints 2; } // O1 * O1
        }
      }
    }
  }
}

Jacobian{
  { Name Jac;
    Case{
      { Region Omega; Jacobian Vol; }
      { Region Bndry; Jacobian Sur; }
    }
  }
}

FunctionSpace{
  { Name HCurl; Type Form1;
    BasisFunction{
      { Name sx; NameOfCoef ux; Function BF_Edge;
        Support Region[{Omega, Bndry}]; Entity EdgesOf[All]; }
    }
  }
}

Formulation{
  { Name FormulationCIM; Type FemEquation;
    Quantity{
      { Name e; Type Local; NameOfSpace HCurl; }
    }
    Equation{
      // Maxwell
      Galerkin{ [          1/Mu0 * Dof{d e}, {d e}];
        In Omega; Jacobian Jac; Integration I1; }
      Galerkin{ [      -Puls[]^2 * Eps0 * Dof{  e}, {  e}];
        In Omega; Jacobian Jac; Integration I1; }

      // IBC
      Galerkin{ [-J[] * Puls[]   * SIbc[] * Dof{  e}, {  e}];
        In Bndry; Jacobian Jac; Integration I1; }
    }
  }
}

Include "./cimResolution.pro"; // Resolution for cim.py

PostProcessing{
  { Name Post; NameOfFormulation FormulationCIM;
    Quantity{
      { Name E; Value{ Term{ [{e}]; In Omega; Jacobian Jac; } } }
    }
  }
}

PostOperation{
  { Name Post; NameOfPostProcessing Post;
    Operation{
      Print[E, OnElementsOf Omega, File fileName];
    }
  }
}
