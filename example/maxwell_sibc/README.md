This is a non-linear eigenvalue problem example. It consists in a spherical
electromagnetic cavity. The wall conductivity is modelled by a Leontovich
surface impedance boundary condition (SIBC). Simply run:

    make

to test cim.py.
The [Makefile](Makefile) will first mesh the geometry [sphere.geo](sphere.geo)
by calling [Gmsh](http://gmsh.info). Afterwards, cim.py is called. The [GetDP](
http://getdp.info) formulation is located in [sphere.pro](sphere.pro).
The [GetDP](http://getdp.info) codes
related to cim.py are located in [cimParameters.pro](cimParameters.pro) and
[cimResolution.pro](cimResolution.pro).

For the default parameters:

    radius = 100 mm
    conductivity = 1e15 S/m

the analytical resonance angular frequency for the fundamental mode should be

    8.22543e9+2.46361e1j

where j is the imaginary unit. This analytical result comes from the reference:

[S. Papantonis and S. Lucyszyn, "Lossy spherical cavity resonators for
stress-testing arbitrary 3D eigenmode solvers," Progress In Electromagnetics
Research, vol. 151, pp. 151-167, 2015.](https://dx.doi.org/10.2528/PIER15031702)
