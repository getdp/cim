Include "sphere.dat";

Point(0) = { 0,  0,  0, cl};

Point(1) = {+R,  0,  0, cl};
Point(2) = { 0, +R,  0, cl};
Point(3) = {-R,  0,  0, cl};
Point(4) = { 0, -R,  0, cl};
Point(5) = { 0,  0, +R, cl};
Point(6) = { 0,  0, -R, cl};

Circle(01) = {1, 0, 2};
Circle(02) = {2, 0, 3};
Circle(03) = {3, 0, 4};
Circle(04) = {4, 0, 1};

Circle(05) = {5, 0, 1};
Circle(06) = {1, 0, 6};
Circle(07) = {6, 0, 3};
Circle(08) = {3, 0, 5};

Circle(09) = {2, 0, 6};
Circle(10) = {6, 0, 4};
Circle(11) = {4, 0, 5};
Circle(12) = {5, 0, 2};

Line Loop(1) = {1, -12, 5};
Line Loop(2) = {1, 9, -6};
Line Loop(3) = {9, 7, -2};
Line Loop(4) = {2, 8, 12};
Line Loop(5) = {11, 5, -4};
Line Loop(6) = {4, 6, 10};
Line Loop(7) = {10, -3, -7};
Line Loop(8) = {3, 11, -8};

Ruled Surface(1) = {1};
Ruled Surface(2) = {2};
Ruled Surface(3) = {3};
Ruled Surface(4) = {4};
Ruled Surface(5) = {5};
Ruled Surface(6) = {6};
Ruled Surface(7) = {7};
Ruled Surface(8) = {8};

Surface Loop(1) = {3, 2, 1, 4, 8, 7, 6, 5};
Volume(1) = {1};

Physical  Volume(1) = {1};
Physical Surface(2) = {1, 2, 3, 4, 5, 6, 7, 8};
Physical   Point(3) = {0};
