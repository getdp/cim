cim.py
======
cim.py is a non-linear eigenvalue solver using the contour integral method
proposed by W.-J. Beyn coupled with a [GetDP](http://getdp.info) formulation.

Usage
-----

    cim.py pro mesh resolution post origin radius

with:

    pro               GetDP .pro file
    mesh              Gmsh .msh file
    resolution        resolution from .pro file
    origin            circular contour origin (complex)
    radius            circular contour radius

Additional optional argument are available. Please run:

    cim.py -h

for more information.

Driving [GetDP](http://getdp.info) from cim.py
----------------------------------------------
cim.py and [GetDP](http://getdp.info) are exchanging the following parameters:

    angularFreqRe  the real part of the frequency
    angularFreqIm  the imaginary part of the frequency
    nRHS           the number of right hand side that should be considered (default: 1)
    b_i()/b~{i}()  the ith right hand side (RHS) of the linear system (first index is 0)
    x_i()/x~{i}()  the ith solution vector of the linear system (first index is 0)
    doInit         flag signalling if an initialisation should be done (default: 0)
    doSolve        flag signalling if a solve should be done for all RHS (default: 0)
    doPostpro      flag signalling if a post-processing should be done (default: 0)
    doApply        flag signalling if x() should be applied to the linear system (default: 0)
    fileName       name of the post-processing file

More information can be found in [bin/solver.py](bin/solver.py). Furthermore,
examples are provided in [example/maxwell\_sibc](example/maxwell_sibc) and
[example/maxwell\_linear](example/maxwell\_linear).

Dependencies
------------
cim.py is a [Python 2.7](https://www.python.org/download/releases/2.7) script.
It relies on the following modules:

    argparse
    numpy
    getdp

The getdp module can be obtained by compiling [GetDP](http://getdp.info).
Among other (and possibly optional) dependencies, [GetDP](http://getdp.info)
relies on [PETSc](https://www.mcs.anl.gov/petsc).
The shell script [dependencies.sh](dependencies.sh) should handle this
automatically if you run:

    ./dependencies.sh

You will need `wget`, `tar`, `gcc`, `g++` and `gfortran` for this script
to work. Don't forget to update your `PATH`, `PYTHONPATH` and `LD_LIBRARY_PATH`
as instructed.

If you want to handle the [PETSc](https://www.mcs.anl.gov/petsc) and [GetDP](
http://getdp.info) libraries yourself, here are the options you will need.
The [PETSc](https://www.mcs.anl.gov/petsc) library should be compiled with the
following features:

    --with-clanguage=cxx
    --with-shared-libraries=1
    --with-x=0
    --with-scalar-type=complex
    --with-mumps-serial=yes
    --with-mpi=0
    --with-mpiuni-fortran-binding=0
    --download-mumps=yes

In other words, [PETSc](https://www.mcs.anl.gov/petsc) should be compiled
without [MPI](http://mpi-forum.org), with complex algebra and with a sequential
version of [MUMPS](http://mumps-solver.org). [GetDP](http://getdp.info) can then
be compiled with the following options:

    ENABLE_BLAS_LAPACK:   ON
    ENABLE_BUILD_DYNAMIC: ON
    ENABLE_BUILD_SHARED:  ON
    ENABLE_KERNEL:        ON
    ENABLE_PETSC:         ON
    ENABLE_WRAP_PYTHON:   ON

If everything went fine, you should end up with the [GetDP](http://getdp.info)
Python module:

    getdp.py

Finally, don't forget to update your `PATH`, `PYTHONPATH` and `LD_LIBRARY_PATH`.

Enjoy!

Reference paper
---------------
[W.-J., Beyn, "An integral method for solving nonlinear eigenvalue problems",
Linear Algebra and its Applications, 2012,436.](
https://doi.org/10.1016/j.laa.2011.03.030)

License
-------
Copyright (C) 2017 N. Marsic, F. Wolf, S. Schoeps and H. De Gersem,
Institut fuer Theorie Elektromagnetischer Felder (TEMF),
Technische Universitaet Darmstadt.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
