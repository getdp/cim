#!/bin/sh
## Dependencies folder ##
#########################
echo "Creating dependencies folder: dep/"
mkdir dep
cd dep

## BLAS ##
##########
echo "Downloading OpenBLAS"
wget http://github.com/xianyi/OpenBLAS/archive/v0.2.19.tar.gz

echo "Extracting OpenBLAS"
tar xf v0.2.19.tar.gz

echo "Entering OpenBLAS"
cd OpenBLAS-0.2.19
BLAS=$(pwd)

echo "Compiling OpenBLAS"
make

echo "Leaving OpenBLAS"
cd ..

## PETSc ##
###########
echo "Downloading PETSc"
wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.7.5.tar.gz

echo "Extracting PETSc"
tar xf petsc-3.7.5.tar.gz

echo "Entering PETSc"
cd petsc-3.7.5
PETSC_DIR=$(pwd)
PETSC_ARCH=linux-gnu-cxx-seq
PETSC=$PETSC_DIR"/"$PETSC_ARCH

echo "Configuring PETSc"
./configure --with-clanguage=cxx --with-shared-libraries=1 --with-x=0 \
            --with-scalar-type=complex --with-mumps-serial=yes --with-mpi=0 \
            --with-mpiuni-fortran-binding=0 --download-mumps=yes \
            --with-blas-lapack-lib=$BLAS/libopenblas.so

echo "Compiling PETSc"
make all

echo "Checking PETSc"
make test

echo "Leaving PETSc"
cd ..

## GetDP ##
###########
echo "Downloading GetDP"
wget http://getdp.info/src/getdp-2.11.0-source.tgz

echo "Extracting GetDP"
tar xf getdp-2.11.0-source.tgz

echo "Entering GetDP"
cd getdp-2.11.0-source

echo "Configuring GetDP"
mkdir build
cd build
cmake -DDEFAULT=0 -DENABLE_BUILD_DYNAMIC=1 -DENABLE_BUILD_SHARED=1 \
      -DENABLE_KERNEL=1 -DENABLE_PETSC=1 -DENABLE_WRAP_PYTHON=1 \
      -DENABLE_BLAS_LAPACK=1 -DBLAS_LAPACK_LIBRARIES=$BLAS/libopenblas.so \
      -DPython_ADDITIONAL_VERSIONS="2.7" ..

echo "Compiling GetDP"
GETDP=$(pwd)
make

echo "Leaving GetDP"
cd ../../..

## PATH ##
##########
echo "   "
echo "+++"
echo "PETSc and GetDP should be now compiled with the required features."
echo "In order to use cim.py, a few additional steps are required."
echo "We still need to add the libraries and the Python module in your path."
echo "Please add the following in your ~/.bashrc file (or equivalent):"
echo "export PATH=\$PATH:"$(pwd)"/bin"
echo "export PYTHONPATH=\$PYTHONPATH:"$GETDP
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:"$PETSC"/lib:"$GETDP":"$BLAS
echo "+++"

## Enjoy ! ##
#############
